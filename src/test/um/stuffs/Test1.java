package um.stuffs;

import org.junit.BeforeClass;
import org.junit.Test;
import tangent.poc.um.ClientListener;
import tangent.poc.um.Consume;
import tangent.poc.um.Publish;
import tangent.poc.um.Subscriber;
import tangent.poc.utils.Message;

import java.util.Date;

/**
 * Created by patricktchankue on 3/7/19.
 */
public class Test1 {

    @BeforeClass
    public static void setUp(){
//        System.setProperty("UMURL", "nsp://localhost:9000");
        System.setProperty("UMURL", "nsp://mdlswarm1:5555");
    }
    @Test
    public void test1() throws Exception {
        Consume consume = new Consume();

        consume.subscribe("initial--2");
    }

    @Test
    public void testSubscribing() throws Exception {
        Subscriber subscriber = new Subscriber("test-channel-1");

    }

    @Test
    public void testSubscribing2() throws Exception {
        Consume consume = new Consume();
        consume.subscribe("initial--1");
    }

    @Test
    public void testPublish() throws Exception {
        Publish publish = new Publish();
        String date = new Date().toString();
        Message message = new Message();
        message.setId(1000);
        message.setMessage(date);
        publish.publishAuditTrail(date);
    }

}
