package um.stuffs;

import org.junit.BeforeClass;
import org.junit.Test;
import tangent.poc.um.queue.QueuePublish;

import java.util.Date;

/**
 * Created by patricktchankue on 5/24/19.
 */
public class QueueT {

    @BeforeClass
    public static void setUp(){
        System.setProperty("UMURL", "nsp://mdlswarm1:5555");
    }

    @Test
    public void test1() throws Exception {
        QueuePublish queuePublish = new QueuePublish("example_q");

        queuePublish.publishQueue("example_q","My nice message to the queue " + new Date());
    }

}
