package tangent.poc.um;

import com.pcbsys.nirvana.client.*;
import org.apache.log4j.Logger;

public class Subscriber implements nEventListener {

    private Logger logger = Logger.getLogger(Subscriber.class.getName());

    private nChannel channel;

    public Subscriber(nChannel channel){
        this.channel = channel;
    }

    public Subscriber(String channel){
        this.channel = getChannel(channel);
    }

    public nChannel getChannel(String name) {
        nSessionAttributes nsa;
        nChannel nc = null;
        try {
            nsa = new nSessionAttributes(System.getProperty("UMURL"));

            // multiplexed
            nSession ns = nSessionFactory.createMultiplexed(nsa);
            logger.info("Connecting... ");
            ns.init();
            logger.info("Connected.");

            nChannelAttributes nca = new nChannelAttributes();
            nca.setName(name);

            nc = ns.findChannel(nca);

        } catch (Exception e) {
            logger.error(e);

        } finally {
            return nc;
        }

    }
    @Override
    public void go(nConsumeEvent nce) {
        String s = new String(nce.getEventData());
        System.out.println(" >>> Text Decryted : " + s);
        logger.info( Thread.currentThread().getName());
        try{
            nce.ack(true);

            channel.purgeEvents(nce.getEventID(), nce.getEventID());

        } catch(Exception e){
            logger.error(e);
        }

    }
}
