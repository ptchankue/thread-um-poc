package tangent.poc.um.channel;

import com.pcbsys.nirvana.client.nConsumeEvent;
import org.apache.log4j.Logger;

/**
 * Created by patricktchankue on 5/23/19.
 */
public class ChannelPublish {
    private static Logger logger = Logger.getLogger(ChannelPublish.class);
    private ChannelUtils channelUtils;

    public ChannelPublish() {
        channelUtils = new ChannelUtils();
    }

    public ChannelPublish(String q) {
        channelUtils = new ChannelUtils(q);
    }


    public void publishChannel(String channelName, String message) throws Exception {
        logger.info("Publish starts: " + message + " to " + channelName);
        if (channelUtils.getMyChannel()==null){
            channelUtils.findChannel(channelName);
        }
        channelUtils.getMyChannel().publish(new nConsumeEvent("TAG", message.getBytes()));

        logger.info("Publish ends");
    }

}
