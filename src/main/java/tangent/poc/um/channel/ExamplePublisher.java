package tangent.poc.um.channel;

import java.util.Date;

/**
 * Created by patricktchankue on 5/25/19.
 */
public class ExamplePublisher {
    public static void main(String[] args) throws Exception {
        ChannelPublish channelPublish = new ChannelPublish("example_channel");
        for (int i =0 ; i < 500; i++){
            channelPublish.publishChannel("example_channel",Thread.currentThread().getName() + " :: Channel " + i + "   " + new Date());
        }
    }
}
