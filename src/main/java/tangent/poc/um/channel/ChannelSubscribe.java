package tangent.poc.um.channel;

import com.pcbsys.nirvana.client.*;

import java.util.UUID;

public class ChannelSubscribe implements nEventListener {
    private ChannelUtils channelUtils;

    public ChannelSubscribe(String channelName) throws Exception {
        channelUtils = new ChannelUtils(channelName);
        nChannel nc = channelUtils.getMyChannel();

        nNamedObject durableSubscriber = nc.createNamedObject(String.valueOf(UUID.randomUUID()), 0, true);
        nConsumeEventFragmentReader reader = new nConsumeEventFragmentReader(this);
        nc.addSubscriber(reader, durableSubscriber);

    }

    public void go(nConsumeEvent event) {
        String s = new String(event.getEventData());
        System.out.println("Consumed event from channel: " + event.getEventID());
        System.out.println(">>> " + s);
    }



    public static void main(String[] args) {
        try {
            new ChannelSubscribe("example_channel");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
