package tangent.poc.um.channel;

import com.pcbsys.nirvana.client.*;

/**
 * Created by patricktchankue on 5/24/19.
 */
public class ChannelUtils {

    private String[] realmName = {"nsp://mdlswarm1:5555"};
    private nSession mySession;
    private nChannel myChannel;

    public ChannelUtils(){
        try {
            initialiseSession();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public ChannelUtils(String channelName){
        try {
            initialiseSession();
            findChannel(channelName);

        } catch (Exception e) {
            if (e.getClass().toString().equalsIgnoreCase("class com.pcbsys.nirvana.client.nChannelNotFoundException")) {
                try {
                    createChannel(channelName);
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            } else {
                e.printStackTrace();
            }

        }
    }
    public void initialiseSession() throws Exception {
        nSessionAttributes nsa = new nSessionAttributes(realmName);
        mySession = nSessionFactory.create(nsa);
        mySession.init();
    }

    public void createChannel(String channelName) throws Exception {
        // creating queue
        nChannelAttributes cattrib = new nChannelAttributes();
        cattrib.setChannelMode(nChannelAttributes.CHANNEL_MODE);
        cattrib.setMaxEvents(0);
        cattrib.setTTL(0);
        cattrib.setType(nChannelAttributes.PERSISTENT_TYPE);
        cattrib.setName(channelName);

        myChannel = mySession.createChannel(cattrib);
    }

    public void findChannel(String channelName) throws Exception {
        // creating queue
        nChannelAttributes cattrib = new nChannelAttributes();
        cattrib.setName(channelName);
        myChannel = mySession.findChannel(cattrib);

    }

    public nChannel getMyChannel() {
        return myChannel;
    }
}
