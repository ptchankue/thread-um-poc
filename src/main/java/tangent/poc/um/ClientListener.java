package tangent.poc.um;

import com.pcbsys.nirvana.client.nChannel;
import com.pcbsys.nirvana.client.nConsumeEvent;
import com.pcbsys.nirvana.client.nEventListener;
import org.apache.log4j.Logger;

public class ClientListener implements nEventListener {
    private Logger logger = Logger.getLogger(ClientListener.class.getName());

    private nChannel channel;

    public ClientListener(nChannel channel){
        this.channel = channel;
    }

    @Override
    public void go(nConsumeEvent nce) {
        String s = new String(nce.getEventData());

        System.out.println(nce.getAttributes());
        System.out.println(nce.getEventTag());

        System.out.println(" >>> Text Decryted : " + s);
        logger.info( Thread.currentThread().getName());
        try{
            nce.ack(true);

            channel.purgeEvents(nce.getEventID(), nce.getEventID());

        } catch(Exception e){
            logger.error(e);
        }

    }
}
