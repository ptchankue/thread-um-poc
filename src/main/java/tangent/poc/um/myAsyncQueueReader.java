package tangent.poc.um;

import com.pcbsys.nirvana.client.*;
import tangent.poc.um.queue.QueueUtils;

public class myAsyncQueueReader implements nEventListener {
    private QueueUtils queueUtils;

    public myAsyncQueueReader(String q) throws Exception {
        queueUtils = new QueueUtils(q);

        nQueueReaderContext ctx = new nQueueReaderContext(this, 10);
        nQueueAsyncReader reader = queueUtils.getMyQueue().createAsyncReader(ctx);
    }

    public void go(nConsumeEvent event) {
        String s = new String(event.getEventData());
        System.out.println("Consumed event " + event.getEventID());
        System.out.println(">>> " + s);
    }



    public static void main(String[] args) {
        try {
            new myAsyncQueueReader("example_q");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
