package tangent.poc.um;

import com.pcbsys.nirvana.client.*;
import org.apache.log4j.Logger;
import tangent.poc.utils.UMAdmin;

import java.io.ByteArrayOutputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;

/**
 * Created by PSielinou on 2017/02/08.
 */
public class Publish {
    Logger logger = Logger.getLogger(Publish.class.getName());

    private static String channelName = "test-channel-1";
    private nChannelAttributes nca;
    private nChannel nc;
    private nSession ns = null;
    private static String umServer = System.getProperty("UMURL");


    byte[] bytesToSend = null;


    public void publishUsingThread(final String toPublish) {
       // new Thread(() -> this.publishAuditTrail(toPublish)).start();
        new Thread(){
            public void run(){
                publishAuditTrail(toPublish);
            }
        }.start();
    }
    public void publishAuditTrail(String toPublish) {

        logger.info("publishAuditTrail");
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutput out;

        try {
            out = new ObjectOutputStream(bos);
            out.writeObject(toPublish);
            out.flush();

            bytesToSend = bos.toByteArray();

            connectToChannel();

            pub(bytesToSend);

        } catch (Exception e) {
            logger.error(e);

        }
    }

    private void pub(byte[] bytesToSend) throws nSessionNotConnectedException, nSessionPausedException, nIllegalArgumentException, nSecurityException, nMaxBufferSizeExceededException {
        logger.info("Publishing...");
        nEventProperties props = new nEventProperties();

        nConsumeEvent nce = new nConsumeEvent("testThreadUM", props, bytesToSend);
        nc.publish(nce);
        logger.info("Published!");
    }

    /**
     * Create a channel when it does not exist on the UM server
     * @param e exception from the connectTochannel method
     */
    public void createChannel(Exception e){
        logger.info(">>> in createChannel(Exception e)");
        if ("com.pcbsys.nirvana.client.nChannelNotFoundException".equalsIgnoreCase(e.getClass().getCanonicalName())) {
            UMAdmin umAdmin = new UMAdmin();
            try {
                nca.setType(nChannelAttributes.MIXED_TYPE);

                nc = ns.createChannel(nca);

                logger.info(">>> Channel created!  " + channelName);
                umAdmin.giveFullAccess(channelName);

            } catch (Exception e1) {
                logger.info(e1);
            }
        }
    }

    /**
     * Attempts the connection to a channel and handles the exception in the case the channels doesn't exist
     */
    public void connectToChannel() {
        nSessionAttributes nsa;

        try {
            nsa = new nSessionAttributes(umServer);
            ns = nSessionFactory.create(nsa);
            logger.info("Connecting... ");
            ns.init();
            logger.info("Connected.");

            nca = new nChannelAttributes();
            nca.setName(channelName);

            nc = ns.findChannel(nca);
        }  catch (Exception e) {
            logger.error(e);

            createChannel(e);
        }
    }

}
