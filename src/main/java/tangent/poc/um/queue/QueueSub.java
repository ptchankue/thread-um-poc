package tangent.poc.um.queue;

import com.pcbsys.nirvana.client.nConsumeEvent;
import com.pcbsys.nirvana.client.nEventListener;
import com.pcbsys.nirvana.client.nQueueAsyncReader;
import com.pcbsys.nirvana.client.nQueueReaderContext;

public class QueueSub implements nEventListener {
    private QueueUtils queueUtils;

    public QueueSub(String q) throws Exception {
        queueUtils = new QueueUtils(q);

        nQueueReaderContext ctx = new nQueueReaderContext(this, 10);
        nQueueAsyncReader reader = queueUtils.getMyQueue().createAsyncReader(ctx);
    }

    public void go(nConsumeEvent event) {
        String s = new String(event.getEventData());
        System.out.println("Consumed event " + event.getEventID());
        System.out.println(">>> " + s);
    }



    public static void main(String[] args) {
        try {
            new QueueSub("example_q");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
