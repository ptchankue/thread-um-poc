package tangent.poc.um.queue;

import java.util.Date;

/**
 * Created by patricktchankue on 5/24/19.
 */
public class PublishTest {


    public static void main(String[] args) throws Exception {
        QueuePublish queuePublish = new QueuePublish();
        for (int i =0 ; i < 1000; i++){
            queuePublish.publishQueue("example_q",Thread.currentThread().getName() + " :: My nice message to the queue " + new Date());
        }
    }
}
