package tangent.poc.um.queue;

import com.pcbsys.nirvana.client.*;
import org.apache.log4j.Logger;
import tangent.poc.um.queue.QueueUtils;

/**
 * Created by patricktchankue on 5/23/19.
 */
public class QueuePublish {
    private static Logger logger = Logger.getLogger(QueuePublish.class);
    private QueueUtils queueUtils;

    public QueuePublish() {
        queueUtils = new QueueUtils();
    }

    public QueuePublish(String q) {
        queueUtils = new QueueUtils(q);
    }


    public void publishQueue(String q, String message) throws Exception {
        logger.info("Publish starts: " + message + " to " + q);
        if (queueUtils.getMyQueue()==null){
            queueUtils.findQueue(q);
        }
        queueUtils.getMyQueue().push(new nConsumeEvent("TAG", message.getBytes()));

        logger.info("Publish ends");
    }

}
