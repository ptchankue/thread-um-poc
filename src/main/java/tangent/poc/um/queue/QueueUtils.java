package tangent.poc.um.queue;

import com.pcbsys.nirvana.client.*;

/**
 * Created by patricktchankue on 5/24/19.
 */
public class QueueUtils {

    private String[] RNAME = {"nsp://mdlswarm1:5555"};
    private nSession mySession;
    private nQueue myQueue;

    public QueueUtils(){
        try {
            initialiseSession();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public QueueUtils(String q){
        try {
            initialiseSession();
            findQueue(q);

        } catch (Exception e) {
            if (e.getClass().toString().equalsIgnoreCase("class com.pcbsys.nirvana.client.nChannelNotFoundException")) {
                try {
                    createQueue(q);
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            } else {
                e.printStackTrace();
            }

        }
    }
    public void initialiseSession() throws Exception {
        nSessionAttributes nsa = new nSessionAttributes(RNAME);
        mySession = nSessionFactory.create(nsa);
        mySession.init();
    }

    public void createQueue(String queueName) throws Exception {
        // creating queue
        nChannelAttributes cattrib = new nChannelAttributes();
        cattrib.setChannelMode(nChannelAttributes.QUEUE_MODE);
        cattrib.setMaxEvents(0);
        cattrib.setTTL(0);
        cattrib.setType(nChannelAttributes.PERSISTENT_TYPE);
        cattrib.setName(queueName);

        myQueue = mySession.createQueue(cattrib);
    }

    public void findQueue(String queueName) throws Exception {
        // creating queue
        nChannelAttributes cattrib = new nChannelAttributes();
        cattrib.setName(queueName);
        myQueue = mySession.findQueue(cattrib);

    }

    public nQueue getMyQueue() {
        return myQueue;
    }
}
