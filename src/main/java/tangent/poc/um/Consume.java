package tangent.poc.um;

import com.pcbsys.nirvana.client.*;
import org.apache.log4j.Logger;
import tangent.poc.utils.UMAdmin;

/**
 * Created by PSielinou on 2017/06/30.
 */
public class Consume {
    private static Logger logger = Logger.getLogger(Consume.class);

    private static String channelName = "test-channel-1";
    private nChannelAttributes nca;
    private nChannel nc;
    private nQueue nq;
    private nSession ns = null;
    private static String umServer = System.getProperty("UMURL");


    public void subscribe(String id){
        logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
        logger.info(">>> Subscribing to " + channelName);
        logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");

        connectToChannel();

        try {

            logger.info(">>> Channel: " + nc);

            nNamedObject durableSubscriber = nc.createNamedObject(id, 0, true);
            nConsumeEventFragmentReader reader = new nConsumeEventFragmentReader(new ClientListener(nc));
            nc.addSubscriber(reader, durableSubscriber);

        } catch (Exception e) {
            logger.error("subscribeToTestThread error", e);
        }
    }

    public void connectToChannel() {
        logger.info(">>> connectToChannel: " + channelName + " On server: " + umServer);
        nSessionAttributes nsa;
        try {
            nsa = new nSessionAttributes(umServer);
            // multiplexed
            ns = nSessionFactory.createMultiplexed(nsa);
            logger.info("Connecting... ");
            ns.init();
            logger.info("Connected.");

            nca = new nChannelAttributes();
            nca.setName(channelName);

            nc = ns.findChannel(nca);

        } catch (Exception e) {
            logger.error(e);

            createChannel(e);
        }
    }

    /**
     * Creating the channel if it doesn't exist
     * @param e
     */
    public void createChannel(Exception e){
        logger.info(">>> in createChannel(Exception e)");
        if ("com.pcbsys.nirvana.client.nChannelNotFoundException".equalsIgnoreCase(e.getClass().getCanonicalName())) {
            UMAdmin umAdmin = new UMAdmin();
            try {
                nca.setType(nChannelAttributes.MIXED_TYPE);

                nc = ns.createChannel(nca);

                logger.info(">>> Channel created!");
                umAdmin.giveFullAccess(channelName);

            } catch (Exception e1) {
                logger.info(e1);
            }
        }
    }
}
