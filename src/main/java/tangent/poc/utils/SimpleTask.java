package tangent.poc.utils;

import org.apache.log4j.Logger;
import tangent.poc.um.Publish;

/**
 * Created by PSielinou on 2017/07/04.
 */
public class SimpleTask implements Runnable {
    private Logger logger = Logger.getLogger(SimpleTask.class);
    private Publish publish = new Publish();
    private String msg = "";

    public SimpleTask(){}
    public SimpleTask(String msg) {
        this.msg = msg;
    }
    @Override
    public void run() {
        // Using our publisher
        publish.publishAuditTrail(msg);
        logger.info("Thread started from Executor: " + Thread.currentThread().getName());
    }
}
