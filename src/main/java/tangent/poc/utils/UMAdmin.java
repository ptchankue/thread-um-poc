package tangent.poc.utils;

import com.pcbsys.nirvana.client.nSessionAttributes;
import com.pcbsys.nirvana.nAdminAPI.nACL;
import com.pcbsys.nirvana.nAdminAPI.nChannelACLEntry;
import com.pcbsys.nirvana.nAdminAPI.nLeafNode;
import com.pcbsys.nirvana.nAdminAPI.nRealmNode;
import org.apache.log4j.Logger;

import java.util.Enumeration;

/**
 * Created by PSielinou on 2017/05/10.
 */
public class UMAdmin {
    String realmName = System.getProperty("UMURL");
    private nRealmNode myRealm;
    private nACL acl = null;
    private nLeafNode leafNode;
    private static Logger logger = Logger.getLogger(UMAdmin.class);

    public void connect() throws Exception {
        logger.info(">>> Connecting...");
        myRealm = new nRealmNode(new nSessionAttributes( realmName ));
        logger.info(">>> Connected!");
        logger.info(">>> Number of channels: " + myRealm.getNoOfChannels());
    }
    public void connect(String realmName) throws Exception {
        this.realmName = realmName;
        //logger.info(">>> Connecting...");
        myRealm = new nRealmNode(new nSessionAttributes( realmName ));
        //logger.info(">>> Connected!");
        //logger.info(">>> Number of channels: " + myRealm.getNoOfChannels());
        acl = myRealm.getACLs();

    }

    private nLeafNode getChannel(String channelName) throws Exception {
        Enumeration enum1 = myRealm.getNodes();

        while(enum1.hasMoreElements()){
            Object obj = enum1.nextElement();
            if(obj instanceof nLeafNode){
                nLeafNode leaf = (nLeafNode)obj;
                logger.info("Leaf: " + leaf.getName());
                if(channelName.equalsIgnoreCase(leaf.getName())){
                    acl = leaf.getACLs();
                    leafNode = leaf;
                    logger.info(channelName + " found!");
                    break;
                }
            }
        }

        return leafNode;
    }
    private void changeACL(nLeafNode leafNode) throws Exception {

        if(null != leafNode){
            acl = leafNode.getACLs();
            nChannelACLEntry channelACLEntry = new nChannelACLEntry("*@*");
            channelACLEntry.setFullPrivileges(true);
            leafNode.addACLEntry(channelACLEntry);
            logger.info(">>> Full access given!");
        } else {
            logger.info(">>> Leaf node is null");
        }
    }
    public void giveFullAccess(String channelName) throws Exception {
        if(null == myRealm){
            this.connect(realmName);
        }
        changeACL( getChannel(channelName));

    }

}
