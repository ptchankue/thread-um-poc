package tangent.poc.utils;

/**
 * Created by PSielinou on 2017/07/04.
 */
public class MyThreadInfo {

    private String name;
    private String state;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
