package tangent.poc.controller;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import tangent.poc.um.Publish;
import tangent.poc.utils.Message;
import tangent.poc.utils.MyThreadInfo;
import tangent.poc.utils.SimpleTask;

import java.lang.management.ManagementFactory;
import java.lang.management.ThreadInfo;
import java.lang.management.ThreadMXBean;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by PSielinou on 2017/06/30.
 */
@RestController
public class POCController {

    private Logger logger = Logger.getLogger(POCController.class);
    private static int i;

    //@Resource(name = "DefaultManagedExecutorService")
    //ManagedExecutorService executor;

    @RequestMapping(value = "/publish")
    public @ResponseBody ResponseEntity publish() {

        Publish publish = new Publish();

        String date = new Date().toString();
        Message message = new Message();
        message.setId(++i);
        message.setMessage(date);

        publish.publishAuditTrail(date);
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(message);
    }

    @RequestMapping(value = "/publish-thread")
    public @ResponseBody ResponseEntity publishThread() {

        Publish publish = new Publish();

        String date = new Date().toString();
        Message message = new Message();
        message.setId(++i);
        message.setMessage(date);

        publish.publishUsingThread(date);
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(message);
    }

    @RequestMapping(value = "/threads")
    public @ResponseBody ResponseEntity allThreads() {

        return ResponseEntity
                .status(HttpStatus.OK)
                .body(generateThreadDump());
    }

    @RequestMapping(value = "/executor")
    public @ResponseBody ResponseEntity<Map<String, String>> executor() {
        //executor.execute(new SimpleTask());
        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.submit(new SimpleTask());
        Map<String, String> response = new HashMap<String, String>();
        response.put("message", "Running with Wildfly executor, check the logs to see the thread's name");
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(response);
    }

    public static  List<MyThreadInfo> generateThreadDump() {

        List<MyThreadInfo> myThreadInfoArrayList = new ArrayList<MyThreadInfo>();

        final ThreadMXBean threadMXBean = ManagementFactory.getThreadMXBean();
        final ThreadInfo[] threadInfos = threadMXBean.getThreadInfo(threadMXBean.getAllThreadIds(), 100);
        System.out.println(threadInfos.length + " threads found in JVM");
        for (ThreadInfo threadInfo : threadInfos) {

            MyThreadInfo myThreadInfo = new MyThreadInfo();
            myThreadInfo.setName(threadInfo.getThreadName());
            myThreadInfo.setState(threadInfo.getThreadState().toString());

            myThreadInfoArrayList.add(myThreadInfo);
        }
        return myThreadInfoArrayList;
    }

}
