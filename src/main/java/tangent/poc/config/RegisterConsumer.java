package tangent.poc.config;

import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import tangent.poc.um.Consume;

import javax.annotation.PreDestroy;

/**
 * Created by PSielinou on 2017/07/03.
 */
@Component
public class RegisterConsumer implements ApplicationListener<ApplicationReadyEvent> {

    @Override
    public void onApplicationEvent(final ApplicationReadyEvent event) {
        Consume consume = new Consume();
        consume.subscribe("initial");
    }

    @PreDestroy
    public void registerShutdownHook() {

    }
}
