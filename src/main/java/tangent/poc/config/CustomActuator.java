package tangent.poc.config;

import org.springframework.boot.actuate.endpoint.Endpoint;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Created by patricktchankue on 8/14/17.
 */
@Configuration
@ComponentScan
public class CustomActuator {

    @Bean
    public static Endpoint exampleEndpoint() {
        return new Endpoint<String>() {
            @Override
            public String getId() {
                return "example";
            }

            @Override
            public boolean isEnabled() {
                return true;
            }

            @Override
            public boolean isSensitive() {
                return false;
            }

            @Override
            public String invoke() {
                return "example";
            }
        };
    }
}
