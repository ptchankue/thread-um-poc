package tangent.poc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.web.SpringBootServletInitializer;

/**
 * Created by PSielinou on 2017/06/30.
 */
@SpringBootApplication
public class Application extends SpringBootServletInitializer {

    public static void main(String[] args) {

        System.setProperty("UMURL", "nsp://mdlswarm1:5555/");

        SpringApplication.run(Application.class, args);
    }

}
