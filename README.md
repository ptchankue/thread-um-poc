#POC 

The aim of this project is to investigate the use of thread in a project deployed in a wildfly container


## Universal Messaging

### Version 9.8

### Version 9.12

##Deployment
```
mvn clean package wildfly:deploy
```

##Running test

Publish without a thread:

```http://localhost:8080/thread-um-poc-1.0/publish```

Explicitly starting a thread:

```http://localhost:8080/thread-um-poc-1.0/publish-thread```

Using the container Thread pool:

```http://localhost:8080/thread-um-poc-1.0/executor```

## Changing configs on the fly

ManagedText...

package UM functionality in jar and put it wildfly